from thconfig.file import FileConfig

async def example_1():
    '''
    FileConfig - a class to handle reading and writing
    configuration data from file
        
    Instantiate FileConfig:
        parameters:
            path: str,
            fetch: bool = True,
            commit: bool = False
            
    fetch:
        parameters:
            self: FileConfig
    '''
    
    # you need to provide file with data { configuration }
    config_path = 'example_1.json'

    # create instance of FileConfig
    config = FileConfig(config_path)

    # load data from configuration file if success
    res: bool = (await config.fetch()).unwrap()

    # get title
    title = config['title']

    # get database server ip
    database = config['database']