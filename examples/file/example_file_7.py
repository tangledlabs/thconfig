from thconfig.file import FileConfig

async def example_7():
    '''
    FileConfig - a class to handle reading and writing
    configuration data from file
        
    Instantiate FileConfig:
        parameters:
            path: str,
            fetch: bool = True,
            commit: bool = False
            
    commit:
        parameters:
            self: FileConfig

    '''
     
    # handling error using try and except blocks
    # trying to create instance of FileConfig by given wrong path
    config = FileConfig(path = 'example_wrong_path')

    # set title
    config.title2 = 'Config Example'

    # standard way to handle exception using try and except blocks
    try:
        # commit
        (await config.commit()).unwrap()
    except:
        pass
