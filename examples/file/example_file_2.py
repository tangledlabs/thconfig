from thconfig.file import FileConfig

async def example_2():
    '''
    FileConfig - a class to handle reading and writing
    configuration data from file
        
    Instantiate FileConfig:
        parameters:
            path: str,
            fetch: bool = True,
            commit: bool = False
            
    commit:
        parameters:
            self: FileConfig
    '''
    
    # you need to provide file with data { configuration }
    config_path = 'example_1.json'

    # create instance of FileConfig
    config = FileConfig(config_path)

    # set title
    config['title'] = 'Config Example'
    config.title2 = 'Config Example'

    # commit changes into file
    (await config.commit()).unwrap()