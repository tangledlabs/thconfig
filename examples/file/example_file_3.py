from thconfig.file import FileConfig


async def example_3():
    '''
    FileConfig - a class to handle reading and writing
    configuration data from file
        
    Instantiate FileConfig:
        parameters:
            path: str,
            fetch: bool = True,
            commit: bool = False
            
    commit:
        parameters:
            self: FileConfig

    fetch:
        parameters:
            self: FileConfig
    '''
    
    # you need to provide file with data { configuration }
    config_path = 'example_1.json'

    # create instance of FileConfig
    config = FileConfig(config_path)

    # set title
    config['title'] = 'Config Example'
    config.title2 = 'Config Example'

    # this function change title in file
    (await config.commit()).unwrap()

    # load data from configuration file if success
    config = FileConfig(path=config_path)
    
    # fetch changes
    res: bool = (await config.fetch()).unwrap()