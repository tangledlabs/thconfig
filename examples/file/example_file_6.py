from thconfig.file import FileConfig

async def example_6():
    '''
    FileConfig - a class to handle reading and writing
    configuration data from file
        
    Instantiate FileConfig:
        parameters:
            path: str,
            fetch: bool = True,
            commit: bool = False
            
    fetch:
        parameters:
            self: FileConfig

    unwrap_or:
        parameters:
            msg: str
    '''
     
    # handling error using unwrap_or function - in case of exception will have default value
    config = FileConfig(path = 'example_wrong_path')
    
    # fetch
    res: bool = (await config.fetch()).unwrap_or('default value in case of exception')
    