from thconfig.file import FileConfig


## Error Handling
# fetch
async def example_4():
    '''
    FileConfig - a class to handle reading and writing
    configuration data from file
        
    Instantiate FileConfig:
        parameters:
            path: str,
            fetch: bool = True,
            commit: bool = False

    fetch:
        parameters:
            self: FileConfig
    '''

    # handling error using try and except blocks
    try:
        config = FileConfig(path = 'example_wrong_path')
        
        # fetch
        res: bool = (await config.fetch()).unwrap()
    except:
        # get Exception FileConfigError
        res = False