from thconfig.file import FileConfig


async def example_8():
    '''
    FileConfig - a class to handle reading and writing
    configuration data from file
        
    Instantiate FileConfig:
        parameters:
            path: str,
            fetch: bool = True,
            commit: bool = False
            
    commit:
        parameters:
            self: FileConfig
    '''

    # handling error using unwrap_value function - returns exception's message as string instead of raising exception
    config = FileConfig(path = 'example_wrong_path')
    
    # set title
    config.title2 = 'Config Example'
    
    # commit
    res: bool = (await config.commit()).unwrap_value()