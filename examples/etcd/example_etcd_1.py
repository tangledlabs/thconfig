from thconfig.http import EtcdConfig


async def example_1():
    '''
    Instantiate EtcdConfig,
    Commit changes into document from etcd 
 
    Instantiate EtcdConfig:
        parameters:
            host: str,
            port: int,
            fetch: bool = True, 
            commit: bool = False
 
    Commit changes:
        parameters:
            self: EtcdConfig
    '''
    
    # you need to provide host and port
    HOST = 'etcd-test'
    PORT = 2379

    # create instance of EtcdConfig
    config = EtcdConfig(host = HOST, port = PORT)

    # set config params for etcd 
    async with EtcdConfig(host = HOST, port = PORT) as config:
        
        # set title
        title = 'Couch Config Example'
        config.title = title

        # set database
        database = {'server': '192.168.1.1'}
        config['database'] = database

        # commit config params to etcd
        (await config.commit()).unwrap()
