from thconfig.http import EtcdConfig


## error Handling commit
async def example_10():
    '''
    Handling exception using unwrap_value function
    that returns exception's message as string and
    doesen't raise exception
    
    Instantiate EtcdConfig,
    Commit changes

    Instantiate EtcdConfig:
        parameters:
            host: str,
            port: int,
            fetch: bool = True, 
            commit: bool = False
            
    commit changes:
        parameters:
            self: EtcdConfig
    '''
    
    # Wrong port
    HOST = 'wrong-host'
    PORT = '1111'

    # create intance EtcdConfig by given wrong URI
    config = EtcdConfig(host = HOST, port = PORT)
 
    # commit and unwraps_value - returns exception's message as string
    (await config.commit()).unwrap_value() 
      