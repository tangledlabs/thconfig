from thconfig.http import EtcdConfig


async def example_2():
    '''
    Instantiate EtcdConfig,
    Commit changes,
    Fetch changes

    Instantiate EtcdConfig:
        parameters:
            host: str,
            port: int,
            fetch: bool = True, 
            commit: bool = False

    Commit changes:
        parameters:
            self: EtcdConfig
            
    Fetch changes:
        parameters:
            self: EtcdConfig
    '''

    # you need to provide host and port
    HOST = 'etcd-test'
    PORT = 2379

    # create instance of EtcdConfig
    config = EtcdConfig(host = HOST, port = PORT)

    # set title
    title = 'Couch Config Example'
    config.title = title

    # set database
    database = {'server': '192.168.1.1'}
    config['database'] = database

    # commit config params to etcd
    commit_ = (await config.commit()).unwrap()

    # fetch
    fetch_ = (await config.fetch()).unwrap()
