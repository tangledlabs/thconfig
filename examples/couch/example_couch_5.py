import random

from thconfig.http import CouchConfig


async def example_5():
    '''
    Instantiate CouchConfig,
    Commit changes,
    Fetch changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False

    Commit changes:
        parameters:
            self: CouchConfig
            
    Fetch changes:
        parameters:
            self: CouchConfig
    '''
    
    # create a new radnom document id
    doc_id: int = random.randint(0, 0xffffffff)

    # uri for couchdb where are configuration data 
    URI = f'http://tangledhub:tangledhub@couchdb-test:5984/thconfig-test/{doc_id}'

    # create intance CouchConfig and set URI property
    config = CouchConfig(URI)

    title = 'Couch Config Example'
    database = {'server': '192.168.1.1'}

    # set title and database
    config['title'] = title
    config['database'] = database

    # commit changes to couchdb
    commit_0 = (await config.commit()).unwrap()
