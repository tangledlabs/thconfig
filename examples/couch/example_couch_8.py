from thconfig.http import CouchConfig


## error handling fetch
async def example_8():
    '''
    Handling exception using unwrap_or function
    that returns custom_message as string and
    doesen't raise exception
    
    Instantiate CouchConfig,
    Fetch changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False

    Fetch changes:
        parameters:
            self: CouchConfig
            
    unwrap_or:
        parameters:
            custom_message: str
    '''
    
    # create intance CouchConfig by given wrong URI
    config = CouchConfig('Example_wrong_uri')
 
    # fetch and unwraps_or - returns custom message as string
    (await config.fetch()).unwrap_or('Handling error with custom message') # Handling error with custom message
      