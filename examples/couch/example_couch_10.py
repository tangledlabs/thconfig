from thconfig.http import CouchConfig


## error Handling commit
async def example_10():
    '''
    Handling exception using unwrap_value function
    that returns exception's message as string and
    doesen't raise exception
    
    Instantiate CouchConfig,
    Commit changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False
            
    commit changes:
        parameters:
            self: CouchConfig
    '''
    
    # create intance CouchConfig by given wrong URI
    config = CouchConfig('Example_wrong_uri')
 
    # commit and unwraps_value - returns exception's message as string
    (await config.commit()).unwrap_value()
