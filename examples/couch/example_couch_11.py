from thconfig.http import CouchConfig


## error Handling commit 
async def example_11():
    '''
    Handling exception using unwrap_or function
    that returns custom_message as string and
    doesen't raise exception
    
    Instantiate CouchConfig,
    Commit changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False

    commit changes:
        parameters:
            self: CouchConfig
            
    unwrap_or:
        parameters:
            custom_message: str
    '''
    
    # create intance CouchConfig by given wrong URI
    config = CouchConfig('Example_wrong_uri')
 
    # commit and unwraps_or - returns custom message as string
    (await config.commit()).unwrap_or('Handling error with custom message') # Handling error with custom message
      