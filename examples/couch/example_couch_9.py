from thconfig.http import CouchConfig


## error handling commit
async def example_9():
    '''
    A standard wat to handling exceptions
    with try and except block
    
    Instantiate CouchConfig,
    Commit changes

    Instantiate CouchConfig:
        parameters:
            uri: str,
            fetch: bool = True,
            commit: bool = False
            
    commit changes:
        parameters:
            self: CouchConfig
    '''
    
    # create intance CouchConfig by given wrong URI
    config = CouchConfig('Example_wrong_uri')

    # standard way to handle exception using try and except blocks
    try:
        # commit
        (await config.commit()).unwrap()
    except:
        pass
    