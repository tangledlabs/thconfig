import pytest

from thconfig.http import EtcdConfig


@pytest.mark.asyncio
async def test_etcd_config():
    '''
    This function tests etcd config
    '''
    HOST = 'etcd-test'
    PORT = 2379
    
    # set etcd config keys and values
    title = 'ETCD Config Example'
    database = {'server': '192.168.1.1'}

    async with EtcdConfig(HOST, PORT, commit=True) as config:
        config.title = title
        config['database'] = database
        print(config.__dict__['_state'])

        assert config['title'] == title
        assert config['database'] == database

    # fetch etcd config keys and values
    async with EtcdConfig(HOST, PORT) as config:
        assert config['title'] == title
        assert config['database'] == database
        assert config['database']['server'] == '192.168.1.1'


@pytest.mark.asyncio
@pytest.mark.xfail(raises=Exception, strict=True)
async def test_etcd_config_error():
    '''
    This function tests etcd config error
    '''
    HOST = 'etcd-test'
    PORT = 2379

    async with EtcdConfig(HOST, PORT, commit=True) as config:
        1 / 0


@pytest.mark.asyncio
async def test_etcd_config_fetch():
    '''
    This function tests etcd fetch data
    '''
    HOST = 'etcd-test'
    PORT = 2379

    # get
    async with EtcdConfig(HOST, PORT) as config:
        assert config['title'] == 'ETCD Config Example'
        assert config['database']['server'] == '192.168.1.1'


@pytest.mark.asyncio
async def test_etcd_config_get_aexit():
    '''
    This function tests etcd aenter and aexit, on shotdown
    '''
    HOST = 'etcd-test'
    PORT = 2379

    config = EtcdConfig(HOST, PORT)

    await config.__aenter__()
    await config.__aexit__(None, None, None)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=TypeError, strict=True)
async def test_etcd_config_fetch_error_invalid_type_host():
    '''
    This function tests fetch data, invalid type host, expects ResultException
    '''
    PORT = 2379
    config = EtcdConfig(['HOST'], PORT)
    (await config.fetch()).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=OSError, strict=True)
async def test_etcd_config_fetch_error_invalid_type_port():
    '''
    This function tests fetch data, invalid type port, expects OSError
    '''
    HOST = 'etcd-test'
    config = EtcdConfig(HOST, ['PORT'])
    (await config.fetch()).unwrap()
