import asyncio

import pytest

from thconfig.http import HTTPConfig


@pytest.mark.asyncio
async def test_http_config():
    config = HTTPConfig()
    assert config
