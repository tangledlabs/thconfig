from tempfile import NamedTemporaryFile

import pytest
from toml.decoder import TomlDecodeError
from yaml.scanner import ScannerError

from thconfig.error import FileConfigError
from thconfig.file import FileConfig


@pytest.mark.asyncio
async def test_file_config_fetch():
    '''
    This function tests fetch config data from file, expect success
    '''
    config_paths = [
        'file/test_files/toml/example.toml',
        'file/test_files/yaml/example.yaml',
        'file/test_files/json/example.json',
        'file/test_files/json5/example.json5',
    ]
    for config_path in config_paths:
        config = FileConfig(config_path)

        res: bool = (await config.fetch()).unwrap()
        assert res

        # title
        title = config['title']
        assert title == 'TOML Example'

        title = config.title
        assert title == 'TOML Example'

        # database server ip
        database = config['database']
        assert database['server'] == '192.168.1.1'


@pytest.mark.asyncio
@pytest.mark.xfail(raises=FileConfigError, strict=True)
async def test_file_config_fetch_error_non_existing_file():
    '''
    This function tests fetch config data from non-existing file, expects FileConfigError
    '''
    config = FileConfig('file/test_files/toml/example_.toml')
    res: bool = (await config.fetch()).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=TomlDecodeError, strict=True)
async def test_file_config_fetch_from_invalid_toml_file():
    '''
    This function tests fetch config data from invalid toml file, expects TomlDecodeError
    '''
    config_paths = [
        'file/test_files/toml/example_1.toml',
    ]
    for config_path in config_paths:
        config = FileConfig(config_path)

        res: bool = (await config.fetch()).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ScannerError, strict=True)
async def test_file_config_fetch_from_invalid_yaml_file():
    '''
    This function tests fetch config data from invalid yaml file, expects ScannerError
    '''
    config_paths = [
        'file/test_files/yaml/example_1.yaml',
    ]
    for config_path in config_paths:
        config = FileConfig(config_path)

        res: bool = (await config.fetch()).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ValueError, strict=True)
async def test_file_config_fetch_from_invalid_json_file():
    '''
    This function tests fetch config data from invalid json file, expects ValueError
    '''
    config_paths = [
        'file/test_files/json/example_1.json',
    ]
    for config_path in config_paths:
        config = FileConfig(config_path)

        res: bool = (await config.fetch()).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ValueError, strict=True)
async def test_file_config_fetch_from_invalid_json5_file():
    '''
    This function tests fetch config data from invalid json5 file, expects ValueError
    '''
    config_paths = [
        'file/test_files/json5/example_1.json5',
    ]
    for config_path in config_paths:
        config = FileConfig(config_path)

        res: bool = (await config.fetch()).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=FileConfigError, strict=True)
async def test_file_config_fetch_error_unsupported_file_type():
    '''
    This function tests fetch config data from unsupported file type, expects FileConfigError
    '''
    config = FileConfig('file/test_files/unsupported/example.csv')
    res: bool = (await config.fetch()).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=FileConfigError, strict=True)
async def test_file_config_commit_error_unsupported_file_type():
    '''
    This function tests commit config data to unsupported file type, expects FileConfigError
    '''
    with NamedTemporaryFile() as f:
        path: str = f'{f.name}.csv'
        config = FileConfig(path=path)
        config['title'] = 'TOML Example'
        config.title2 = 'TOML Example'
        (await config.commit()).unwrap()


@pytest.mark.asyncio
async def test_file_config_commit_fetch():
    '''
    This function tests commit and fetch config data from file, expect success
    '''
    with NamedTemporaryFile() as f:
        #
        # commit
        #
        supported_file_types = [
            'toml',
            'yaml',
            'json',
            'json5',
        ]
        for file_type in supported_file_types:
            path: str = f'{f.name}.{file_type}'

            config = FileConfig(path=path)
            config['title'] = 'Config Example'
            config.title2 = 'Config Example'
            (await config.commit()).unwrap()

            #
            # fetch
            #
            config = FileConfig(path=path)
            res: bool = (await config.fetch()).unwrap()
            assert res

            # title
            title = config['title']
            assert title == 'Config Example'

            title2 = config.title2
            assert title2 == 'Config Example'


@pytest.mark.asyncio
async def test_file_config_fetch_context_manager():
    '''
    This function tests file config using context manager, expect success
    '''
    config_paths = [
        'file/test_files/toml/example.toml',
        'file/test_files/yaml/example.yaml',
        'file/test_files/json/example.json',
        'file/test_files/json5/example.json5',
    ]
    for config_path in config_paths:
        async with FileConfig(config_path) as config:
            # title
            title = config['title']
            assert title == 'TOML Example'

            title = config.title
            assert title == 'TOML Example'

            # database server ip
            database = config['database']
            assert database['server'] == '192.168.1.1'
